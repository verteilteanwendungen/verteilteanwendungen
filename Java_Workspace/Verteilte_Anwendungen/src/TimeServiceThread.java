	import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TimeServiceThread extends Thread{

	public static Socket socket;
	public static ServerSocket serverSocket;

	public void socketThread(Socket socketIn){
		socket = socketIn ;
	}
	
	public void run(){
		
		boolean stop = false;
		
		while (!stop) {
			
			try {
				
				
				OutputStream output = socket.getOutputStream();
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
				InputStream input = socket.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(input));
				
				writer.write("time service");
				writer.newLine();
				writer.flush();

					// System.out.println("Inside");
					try {
						while (!stop){
							String s = reader.readLine();

							if (s.equalsIgnoreCase("date")) {
								System.out.println("Inside Date");
								writer.write(Clock.date());
								writer.newLine();
								writer.flush();
							} else if (s.equalsIgnoreCase("time")) {
								System.out.println("Inside Time");
								writer.write(Clock.time());
								writer.newLine();
								writer.flush();
							} else {
								stop = true;
								socket.close();
								System.out.println("Kill it hardly");
							}

						}
				
					} catch (IOException e) {
						// TODO Auto-generated catch block
						//e.printStackTrace();
					}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
	}
	
}
