import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class TimeServiceMultithreaded {

	public static ServerSocket serverSocket;
	public static Socket socket;

	public static void startServerSocket() {

		try {
			serverSocket = new ServerSocket(7500);
			
			while (true){
			socket = serverSocket.accept();
			
			TimeServiceThread t1 = new TimeServiceThread();
			t1.setName("TimeServiceThread ");
			t1.socketThread(socket);
			t1.start();
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		startServerSocket();
		// System.out.println(Clock.date());
	}
}
