import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class TimeServiceClient {

	public static ServerSocket serverSocket;
	public static Socket socket;
	public static String dateServer;
	public static String timeServer;

	/*
	 * public void getDate(){ dateServer = TimeService.date; }
	 * 
	 * public void getTime(){ timeServer = TimeService.time; }
	 */

	public static String dateFromServer(String ipAdresse) {

		try {
			Socket socket = new Socket(ipAdresse, 7500);

			OutputStream output = socket.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
			InputStream input = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));

			String s = reader.readLine();
			System.out.println("Write Date");
			if (s.equals("time service")){
			writer.write("date");
			writer.newLine();
			writer.flush();

			dateServer = reader.readLine();

			writer.write("end");
			writer.newLine();
			writer.flush();
		    }

			System.out.println("Zeit: " + dateServer);

		//	socket.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return dateServer;


	}

	public static String timeFromServer(String ipAdresse) {

		try {
			Socket socket = new Socket(ipAdresse, 7500);

			OutputStream output = socket.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
			InputStream input = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));

			String s = reader.readLine();
			System.out.println("Write Time");
			if (s.equals("time service")){
			writer.write("time");
			writer.newLine();
			writer.flush();

			timeServer = reader.readLine();

			writer.write("end");
			writer.newLine();
			writer.flush();
		    }

			System.out.println("Zeit: " + timeServer);

		//	socket.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		return timeServer;

	}

	public static void main(String[] args) {
		dateFromServer("127.0.0.1");
		timeFromServer("127.0.0.1");
	}
}
