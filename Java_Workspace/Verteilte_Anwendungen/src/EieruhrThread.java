
public class EieruhrThread extends Thread {
	
	int time;
	String outText;
	
	public void setTime(int choosetime){
		time = choosetime;
	}
	
	public void setString(String out)
	{
		outText = out;
	}
	
	public void run(){
		Eieruhr.schlafen(time);
		System.out.println(outText + " " + time + " seconds");
	}
}
