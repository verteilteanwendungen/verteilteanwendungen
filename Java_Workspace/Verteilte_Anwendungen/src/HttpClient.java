import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;

public class HttpClient {

	public static void get(String url) {
		try {
			Boolean stop = false;

			String ipURL = new URL(url).getHost();
			int portURL = new URL(url).getPort();

			if (portURL == -1) {
				portURL = 80;
			}

			Socket socket = new Socket(ipURL, portURL);

			OutputStream output = socket.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
			InputStream input = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));

			writer.write("GET " + url + " HTTP/1.0");
			writer.newLine();
			writer.write("Host:");
			writer.newLine();
			writer.newLine();
			writer.flush();

			while (!stop) {

				String s = reader.readLine();

				if (s != null) {
					System.out.println(s);
				} else {
					stop = true;
				}
			}

		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean checkURL(String url) {

		boolean urlCorrect = true;

		try {
			String ipURL = new URL(url).getHost();
			int portURL = new URL(url).getPort();

			if (portURL == -1) {
				portURL = 80;
			}

			Socket socket = new Socket(ipURL, portURL);

			OutputStream output = socket.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
			InputStream input = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));

			writer.write("GET " + url + " HTTP/1.0");
			writer.newLine();
			writer.write("Host:");
			writer.newLine();
			writer.newLine();
			writer.flush();

			String d = reader.readLine();

			System.out.println(d);

			if (d.contains("OK") || d.contains("200")) {
				System.out.println("true");
				urlCorrect = true;
			} else {
				System.out.println("false");
				urlCorrect = false;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return urlCorrect;
	}

	public static String urlToString(String url) {

		String urlString = "";

		try {
			Boolean stop = false;

			String ipURL = new URL(url).getHost();
			int portURL = new URL(url).getPort();

			if (portURL == -1) {
				portURL = 80;
			}

			Socket socket = new Socket(ipURL, portURL);

			OutputStream output = socket.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
			InputStream input = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));

			writer.write("GET " + url + " HTTP/1.0");
			writer.newLine();
			writer.write("Host:");
			writer.newLine();
			writer.newLine();
			writer.flush();

			while (!stop) {

				String s = reader.readLine();

				if (s != null) {
					urlString = urlString + " " + s;

				} else {
					stop = true;
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(urlString);

		return urlString;

	}

	public static String urlDate(String url) {

		String urlDate = "";

		try {
			Boolean stop = false;

			String ipURL = new URL(url).getHost();
			int portURL = new URL(url).getPort();

			if (portURL == -1) {
				portURL = 80;
			}

			Socket socket = new Socket(ipURL, portURL);

			OutputStream output = socket.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
			InputStream input = socket.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));

			writer.write("GET " + url + " HTTP/1.0");
			writer.newLine();
			writer.write("Host:");
			writer.newLine();
			writer.newLine();
			writer.flush();

			while (!stop) {

				String s = reader.readLine();

				if (s.contains("Date: ")) {
					urlDate = s;
					System.out.println(urlDate);
					stop = true;
				}
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return urlDate;
	}

	public static void main(String[] args) {
		get("http://www.google.de/");
		checkURL("http://www.google.de/");
		urlToString("http://www.google.de/");
		urlDate("http://www.google.de/");

	}
}
